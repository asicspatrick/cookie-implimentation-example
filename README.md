# README #

Sample EU cookie consent implementation.

### What is this repository for? ###

* EU
* Cookie
* Consent

### How do I get set up? ###

* Clone repo
* run on fancy web server
* or, use `python -m SimpleHTTPServer 8000` from the repo root folder to test locally without fancy web server

### How do I use this? ###

* start with either the index.html page or other-page.html in a browser access through a web server
* Click around
* There are some helpful status messages in the console.log
* Cookies can be easily reset to restart the test using the reset link

### Who do I talk to? ###

* Patrick
